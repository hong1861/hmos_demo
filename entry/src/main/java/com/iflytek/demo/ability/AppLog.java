package com.iflytek.demo.ability;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class AppLog {

    public static void d(String tag, String log) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00101, tag);
        HiLog.debug(label, log);
    }

    public static void i(String tag, String log) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00101, tag);
        HiLog.info(label, log);
    }

    public static void w(String tag, String log) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00101, tag);
        HiLog.warn(label, log);
    }

    public static void e(String tag, String log) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00101, tag);
        HiLog.error(label, log);
    }

}
