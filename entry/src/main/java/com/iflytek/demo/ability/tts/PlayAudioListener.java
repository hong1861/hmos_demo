package com.iflytek.demo.ability.tts;


/**
 * @author zhhuang10
 * @version 1.0
 * @description 播放回调
 * @date 2021/4/29
 */
public interface PlayAudioListener {


    /**
     * 播放开始回调
     */
    void onPlayStart();


    /**
     * 播放结束回调
     */
    void onPlayCompletion();
}
