package com.iflytek.demo.ability;

import okhttp3.OkHttpClient;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.concurrent.TimeUnit;

/**
 * @author zhhuang10
 * @version 1.0
 * @description
 * @date 2021/6/30
 */
public class Tools {

    private static OkHttpClient client;

    public static OkHttpClient okHttp() {
        if (client == null) {
            client = new OkHttpClient.Builder()
                    .connectTimeout(10L, TimeUnit.SECONDS)
                    .readTimeout(10L, TimeUnit.SECONDS)
                    .build();
        }
        return client;
    }


    public static String base64(byte[] src) {
        return Base64.getEncoder().encodeToString(src);
    }


    public static String base64(String str) {
        return Base64.getEncoder().encodeToString(str.getBytes());
    }


    public static byte[] base64Decode(String str) {
        return base64Decode(str.getBytes());
    }

    public static byte[] base64Decode(byte[] bytes) {
        return Base64.getDecoder().decode(bytes);
    }

    public static byte[] sha256(byte[] src) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(src);
            return digest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static byte[] hmacsha256(byte[] input, byte[] key) {
        byte[] bytes;
        try {
            Mac mac = Mac.getInstance("hmacsha256");
            SecretKeySpec spec = new SecretKeySpec(key, "hmacsha256");
            mac.init(spec);
            bytes = mac.doFinal(input);
        } catch (Exception var5) {
            bytes = new byte[]{8};
        }
        return bytes;
    }

    public static boolean append(File file, byte[] data) {
        if (file == null || data == null) {
            return false;
        }
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            BufferedSink sink = Okio.buffer(Okio.appendingSink(file));
            sink.write(data);
            sink.flush();
            sink.close();
            return true;
        } catch (IOException var4) {
            var4.printStackTrace();
            return false;
        }
    }


    public static byte[] readData(File file, int index, int len) {
        if (file == null) {
            return null;
        }
        byte[] bytes = new byte[(int) Math.min(len, file.length() - (long) index)];
        byte[] returnByte;
        try {
            BufferedSource source = Okio.buffer(Okio.source(file));
            source.skip(index);
            source.readFully(bytes);
            source.close();
            returnByte = bytes;
        } catch (IOException var10) {
            var10.printStackTrace();
            returnByte = null;
        }
        return returnByte;
    }

    /**
     * 给file追加数据
     */
    public static void appendFile(File file, byte[] buffer) {
        try {
            if (!file.exists()) {
                boolean b = file.createNewFile();
            }
            RandomAccessFile randomFile = new RandomAccessFile(file, "rw");
            randomFile.seek(file.length());
            randomFile.write(buffer);
            randomFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
