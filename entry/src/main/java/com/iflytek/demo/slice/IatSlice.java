package com.iflytek.demo.slice;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.value.LottieAnimationViewData;
import com.iflytek.demo.ResourceTable;
import com.iflytek.demo.ability.iat.IatClient;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * 识别页面
 */
public class IatSlice extends BaseSlice {

    // 语音听写客户端
    private IatClient iatClient;
    // EventHandler
    private final EventHandler handler = new EventHandler(EventRunner.getMainEventRunner());
    // lottie动画 用来指示录音状态
    private LottieAnimationView lottieView;
    private TextField textField;
    private boolean isListening;
    private Text iatBtn;

    /**
     *
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_iat);
        initView();
        initIatClient();
    }

    private void initView() {
        iatBtn = (Text) findComponentById(ResourceTable.Id_iatBtn);
        textField = (TextField) findComponentById(ResourceTable.Id_textField);
        lottieView = (LottieAnimationView) findComponentById(ResourceTable.Id_lottie);
        {
            LottieAnimationViewData data = new LottieAnimationViewData();
            data.setFilename("voice.json");
            data.setRepeatCount(-1);
            data.autoPlay = true;
            lottieView.setAnimationData(data);
        }
        lottieView.setVisibility(Component.INVISIBLE);
        iatBtn.setClickedListener(component -> {
            if (isListening) {
                showIdleUi();
                stopListening();
            } else {
                showListeningUi();
                startListening();
            }
        });
    }

    private void initIatClient() {
        iatClient = new IatClient.Builder()
                .setAppId("5f86e9f9")
                .setApiKey("21ea6d9699614133c0a58c025425d44a")
                .setApiSecret("188a52809d7a672b54557bdfa6adc30e")
                .setLang("zh_cn")
                .setAccent("mandarin")
                .build();
        iatClient.setCallBack(new IatClient.CallBack() {
            @Override
            public void onStart() {
                // 已经连接服务端 开始传送音频，录音机开启
                handler.postTask(() -> lottieView.setVisibility(Component.VISIBLE));
            }

            @Override
            public void onResult(String str, boolean isComplete) {
                handler.postTask(() -> {
                    textField.append(str);
                    // 结果已经全部返回
                    if (isComplete) {
                        showIdleUi();
                    }
                });
            }

            @Override
            public void onError(int code) {
                handler.postTask(() -> {
                    showToast("onError code = " + code);
                    showIdleUi();
                });
            }
        });
    }

    /**
     * 录音中ui
     */
    private void showListeningUi() {
        iatBtn.setText("停止识别");
        textField.setText("");
        isListening = true;
    }

    /**
     * 显示录音结束的ui
     */
    private void showIdleUi() {
        isListening = false;
        iatBtn.setText("开始识别");
        lottieView.setVisibility(Component.INVISIBLE);
    }

    private void startListening() {
        if (iatClient == null) {
            return;
        }
        iatClient.startListening();
    }

    private void stopListening() {
        if (iatClient == null) {
            return;
        }
        iatClient.stopListening();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
