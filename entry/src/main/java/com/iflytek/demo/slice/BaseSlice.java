package com.iflytek.demo.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.window.dialog.ToastDialog;

public class BaseSlice extends AbilitySlice {

    /**
     * ToastDialog
     */
    private ToastDialog toast;

    /**
     * showToast
     *
     * @param text toast文本
     */
    protected void showToast(String text) {
        if (toast != null && toast.isShowing()) {
            toast.cancel();
        }
        toast = new ToastDialog(getContext());
        toast.setText(text);
        toast.show();
    }
}
