package com.iflytek.demo.slice;

import com.iflytek.demo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;

public class MainAbilitySlice extends AbilitySlice {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initView();
    }

    private void initView() {
        final Text record = (Text) findComponentById(ResourceTable.Id_text_record);
        final Text iatBtn = (Text) findComponentById(ResourceTable.Id_text_iat);
        final Text ttsBtn = (Text) findComponentById(ResourceTable.Id_text_tts);
        final Text transBtn = (Text) findComponentById(ResourceTable.Id_text_trans);

        // 录音与播放
        record.setClickedListener(component -> {
            present(new RecordSlice(), new Intent());
        });
        // 语音听写（WebAPI版）
        iatBtn.setClickedListener(component -> {
            present(new IatSlice(), new Intent());
        });
        // 语音合成（WebAPI版）
        ttsBtn.setClickedListener(component -> {
            present(new TtsSlice(), new Intent());
        });
        // 翻译
        transBtn.setClickedListener(component -> {
            present(new TransSlice(), new Intent());
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
