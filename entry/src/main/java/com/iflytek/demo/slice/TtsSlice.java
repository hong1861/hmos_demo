package com.iflytek.demo.slice;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.value.LottieAnimationViewData;
import com.iflytek.demo.ResourceTable;
import com.iflytek.demo.ability.tts.TtsClient;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * 合成页面
 */
public class TtsSlice extends BaseSlice {

    private TextField textField;
    private TtsClient ttsClient;
    private LottieAnimationView lottieView;
    // EventHandler
    private final EventHandler handler = new EventHandler(EventRunner.getMainEventRunner());

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_tts);
        initView();
    }

    private void initView() {

        lottieView = (LottieAnimationView) findComponentById(ResourceTable.Id_lottie);
        {
            LottieAnimationViewData data = new LottieAnimationViewData();
            data.setFilename("voice.json");
            data.setRepeatCount(-1);
            data.autoPlay = true;
            lottieView.setAnimationData(data);
        }
        lottieView.setVisibility(Component.INVISIBLE);

        textField = (TextField) findComponentById(ResourceTable.Id_textField);
        final Text cleanBtn = (Text) findComponentById(ResourceTable.Id_text_clean);
        final Text ttsBtn = (Text) findComponentById(ResourceTable.Id_text_tts);

        cleanBtn.setClickedListener(component -> {
            textField.setText("");
        });
        ttsBtn.setClickedListener(component -> {
            tts();
        });
    }

    private void tts() {
        if (ttsClient != null) {
            ttsClient.cancel();
        }
        if (textField.getText().isEmpty()) {
            return;
        }
        ttsClient = new TtsClient(getContext());
        ttsClient.setAppId("5f86e9f9");
        ttsClient.setApiKey("21ea6d9699614133c0a58c025425d44a");
        ttsClient.setApiSecret("188a52809d7a672b54557bdfa6adc30e");
        ttsClient.setSpeaker("xiaoyan");
        ttsClient.setCallBack(new TtsClient.CallBack() {
            @Override
            public void onResult(byte[] audio, boolean isComplete) {

            }

            @Override
            public void onPlayStart() {
                handler.postTask(() -> lottieView.setVisibility(Component.VISIBLE));
            }

            @Override
            public void onPlayCompletion() {
                handler.postTask(() -> lottieView.setVisibility(Component.INVISIBLE));
            }

            @Override
            public void onError(int code) {
                handler.postTask(() -> {
                    showToast("onError code = " + code);
                    lottieView.setVisibility(Component.INVISIBLE);
                });
            }
        });
        ttsClient.speak(textField.getText());
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
